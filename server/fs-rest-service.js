(function () {
    "use strict";

    var restify = require('restify');
//    var request = require("request");
    var requestify = require('requestify');
    var _ = require('underscore');
    var qs = require('querystring');

    var ip_addr = '127.0.0.1';
    var port = '8080';

    var server = restify.createServer({
        name: "fsVenues"
    });
    server.use(restify.queryParser())
        .use(restify.bodyParser())
        .use(restify.CORS());

    function coreResponse(res, next, success, err) {
//        console.log('Response success ' + success);
//        console.log('Response error ' + err);
        if (success) {
            res.send(201, success);
            return next();
        } else {
            return next(err);
        }
    }

    function fsCall(parameters, callback, callbackParams) {
        //https://api.foursquare.com/v2/venues/search?query=melkweg&intent=browse&near=Amsterdam&radius=50&client_id=GP1QBFAH0OU3BS2BRUTUINEHVQLW4GD5DLSODWRRWPYHQZ0Y&client_secret=WNSDDCHV5CXFQXYJRPGTQ4NMNKEWTFB1HK323H4GIZIIWZDG&v=20140307
        var url = "https://api.foursquare.com/v2/venues/search";
        var generalOptions = {
            v: 20140307,
            client_secret: "WNSDDCHV5CXFQXYJRPGTQ4NMNKEWTFB1HK323H4GIZIIWZDG",
            client_id: "GP1QBFAH0OU3BS2BRUTUINEHVQLW4GD5DLSODWRRWPYHQZ0Y",
            intent: "browse",
            radius: 50,
            near: "Amsterdam",
            limit: 10,
        };
        var queryObj = _.extend(generalOptions, parameters);
        console.log(queryObj);
        var urlFinal = url + "?" + qs.stringify(queryObj);
        console.log(urlFinal);
        requestify.get(urlFinal)
            .then(function (response) {
                console.log(response.getBody());
                return callback(response, callbackParams);
            });
    }

    function findVenuesByNameCallback(res, params) {
        return coreResponse(params.res, params.next, {"result": res.getBody()});
    }

    function findVenuesByName(req, res, next) {
        if (!req.params.query) {
            return coreResponse(res, next, false,
                new restify.MissingParameterError("need a 'query' parameter like '...fsVenues?query=schnitzel'"));
        } else {
            var query = req.params.query;
            if (req.params.city && req.params.city != "") {
                var city = req.params.city;
                fsCall({query: query, near: city}, findVenuesByNameCallback, {res: res, next: next});
            } else {
                fsCall({query: query}, findVenuesByNameCallback, {res: res, next: next});
            }

        }
    }

    function getVenue(req, res, next) {
        var venueID = req.params.venueID;
        return coreResponse(res, next, {"venueID": venueID});
    }

    function notImplemented(req, res, next) {
        return coreResponse(res, next, {"sorry": "not implemented"});
    }

    var PATH = "/fsVenues";
    server.get({path: PATH, version: '1'}, findVenuesByName);
//    server.get({path: PATH + '/:venueID', version: '1'}, getVenue);
    server.get({path: PATH + '/:venueID', version: '1'}, notImplemented);
    server.post({path: PATH, version: '1'}, notImplemented);
    server.put({path: PATH, version: '1'}, notImplemented);
    server.del({path: PATH + '/:venueID', version: '1'}, notImplemented);

    server.listen(port, ip_addr, function () {
        console.log('%s listening at %s ', server.name, server.url);
    });

})();
