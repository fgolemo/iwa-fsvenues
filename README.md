# Foursquare Venue Search
## Prototype app for course "Intelligent Web Applications" @ VU Amsterdam

**Demo Video**
[./src/master/iwa-ex3-demo.mp4](https://bitbucket.org/fgolemo/iwa-fsvenues/src/master/iwa-ex3-demo.mp4)

**Requirements**

* 2 Webservers: one NodeJS and one of your choice (I took my default XAMPP stack)

* Node modules: restify, requestify, underscore, querystring


**Setting up**

Place the project directory in your secondary webserver's public folder. For me that was `/opt/lampp/htdocs`.
Change directory into that new folder, i.e.

    cd /opt/lampp/htdocs/iwa-ex3

Run the node server:

    node ./server/fs-rest-service.js

Run the other server. For me, again, that was `sudo /opt/lampp/lampp start`.
That's it. Now you can either check out the backend or the frontend.

    Backend: http://localhost:8080/fsVenues?query=SOMETHING[&city=SOMETHING]

    Frontend: http://localhost/iwa-ex3/app/index.html

**Usage**

In the frontend you can enter something for the query and it will load the results from the webservice,
and that will in turn fetch it from Foursquare. The matches will be listed on the left side of the page.
 If you click any link on the left side, it will add a marker to the Google map and pan the center to
 that new position.
