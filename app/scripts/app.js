'use strict';

angular.module('iwaEx3App', [
        'restify',
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngRoute'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    });
//    .factory('fsVenues', ['$resource', function($resource){
//            return $resource('http://localhost:8080/fsVenues?query=:query', {}, {
//                query: {method:'GET', params:{query:'@phones'}, isArray: true}
//            });
//        }]);

