'use strict';

angular.module('iwaEx3App')
    .controller('MainCtrl', ["$scope", "$resource", function ($scope, $resource) {

        $scope.gmapsInit = function () {
            var mapOptions = {
                center: new google.maps.LatLng(52.3747158,4.8986166),
                zoom: 12
            };
            $scope.map = new google.maps.Map(document.getElementById("map-canvas"),
                mapOptions);
        };

        var venueAPI = $resource('http://localhost:8080/fsVenues?query=:query&city=:city',
            {query: '@q', city: ''});

        $scope.search = function () {
//            console.log($scope.city);
//            console.log($scope.query);
            venueAPI.get({query: $scope.query, city: $scope.city}, function (data) {
                if (data && data.result && data.result.response && data.result.response.venues) {
                    //console.log(data.result.response.venues);
                    $scope.results = data.result.response.venues;
                } else {
                    console.log("Error while fetching data");
                    console.log(data);
                }
            });
        };

        $scope.marker = false;

        $scope.showMarker = function (venue) {
            if ($scope.marker)
                $scope.marker.setMap(null);

            var pos = new google.maps.LatLng(venue.location.lat, venue.location.lng);
            $scope.marker = new google.maps.Marker({
                animation: google.maps.Animation.DROP,
                position: pos
            });
            $scope.marker.setMap($scope.map);
            $scope.map.setZoom(13);
            $scope.map.panTo($scope.marker.position);
        }

        $scope.results = [];
        $scope.gmapsInit();
    }]);
